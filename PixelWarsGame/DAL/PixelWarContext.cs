﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class PixelWarContext : DbContext
    {
        public PixelWarContext()
            :base("PixelDB")
        {

        }
        public DbSet<Pixel> Pixels { get; set; }
    }
}
