﻿using DAL;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;

namespace PixelWarsGame.Hubs
{
    public class PointerHub : Hub
    {
        PixelWarContext PixelWarContext { get; set; }

        public PointerHub()
        {
            PixelWarContext = new PixelWarContext();
        }

        public override Task OnConnected()
        {
            
            var pixels = PixelWarContext.Pixels.ToList().OrderBy(c => c.DateTime);
            var result = JsonConvert.SerializeObject(pixels);    
            Clients.Caller.drawAllPointsWithinAllTimeForUser(result);
            return base.OnConnected();
        }

        public void Refresh(int x, int y, string color)
        {
            Clients.Others.addPixel(x, y, color);
            SaveToDb(x, y, color);
        }

        public void SaveToDb(int x, int y, string color)
        {
            var pixel = new Pixel()
            {
                X = x,
                Y = y,
                Color = color,
                DateTime = DateTime.Now
            };
            PixelWarContext.Pixels.Add(pixel);
            PixelWarContext.SaveChanges();
        }


    }
}